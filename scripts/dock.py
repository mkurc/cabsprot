import argparse
import subprocess
import os
import logging
import shutil
import glob
import datetime
import numpy as np

from mollib import atom, trafile, utils, cabs


class App:

    def __init__(self, **kwargs):
        self.workdir = kwargs.get('workdir')
        self.receptor = kwargs.get('receptor')
        self.ligand = kwargs.get('ligand')
        self.ligand_restraints_weight = kwargs.get('ligand_restraints_weight')
        self.contact_cutoff = kwargs.get('contact_cutoff')
        self.min_contacts = kwargs.get('min_contacts')
        self.clusters = kwargs.get('clusters')
        self.load_cbs = kwargs.get('load_cbs')
        output_pdb = kwargs.get('output_pdb')
        self.save_trajectory = 'T' in output_pdb or 'A' in output_pdb
        self.save_replicas = 'R' in output_pdb or 'A' in output_pdb
        self.save_clusters = 'C' in output_pdb or 'A' in output_pdb
        self.save_medoids = 'M' in output_pdb or 'A' in output_pdb
        self.reference = kwargs.get('reference')

        if self.load_cbs is None:
            self.cabsdock_args = {
                '-i': self.receptor,
                '-P': f'{self.ligand} keep random',
                '-w': self.workdir,
                '--ca-rest-file': os.path.join(self.workdir, 'restr.txt'),
                '-S': '',       # save .cbs
                '-v': '4',      # verbosity: DEBUG
                '-o': 'N',      # output pdbs: NONE
                '--log': '',    # logging to file
                '-D': 0.25,     # distance between replicas
                '-r': 20,       # number of replicas
                '-a': 20,       # annealing cycles
                '-y': 25,       # simulation cycles
                '-s': 100,      # steps between frames
                '-b': 1.5,      # side chains' interaction scaling
            }

            if kwargs['cabsdock']:
                for kwarg in kwargs['cabsdock']:
                    if '=' in kwarg:
                        arg, val = kwarg.split('=')
                        arg = '-' + arg
                    else:
                        arg = '-' + kwarg
                        val = ''
                    self.cabsdock_args[arg] = val

    def run(self):

        receptor = self.get_atoms(self.receptor)
        ligand = self.get_atoms(self.ligand)

        # build trajectory template
        taken_chains = ''.join(receptor.ca.chains.keys())
        for ch in ''.join(ligand.ca.chains.keys()):
            if ch in taken_chains:
                next_letter = utils.next_letter(taken_chains)
                taken_chains += next_letter
                ligand.chains[ch].update_chain_id(next_letter)
                logging.debug(f'Ligand chain {ch} -> {next_letter}')
        try:
            del ligand.chains
        except AttributeError:
            pass
        trajectory_template = atom.Atoms(receptor.ca.atoms + ligand.ca.atoms)

        # generate ligand restraints
        logging.info('Generating ligand restraints')
        filename = os.path.join(self.workdir, 'restr.txt')
        self.save_restraints(
            atoms=ligand,
            filename=filename,
            weight=self.ligand_restraints_weight
        )
        logging.debug(f'Restraints saved in "{filename}"')

        if self.load_cbs:
            traj_filename = self.load_cbs
        else:
            # run CABSdock
            logging.info('Running CABSdock')
            starttime = datetime.datetime.now()
            proc = self.run_cabsdock()
            for dirname in ['output_data', 'output_pdbs', 'plots']:
                shutil.rmtree(os.path.join(self.workdir, dirname), ignore_errors=True)
            endtime = datetime.datetime.now()
            logging.debug(f'CABSdock run complete in {datetime.timedelta(seconds=(endtime - starttime).seconds)}')
            traj_filename = glob.glob(os.path.join(self.workdir, '*.cbs'))[-1]

        # load trajectory
        trajectory = trafile.Trafile.load(traj_filename)
        logging.info('Loading trajectory')
        logging.debug(f'Trajectory loaded from "{traj_filename}"')

        # rebuild trajectory to CABS representation
        logging.info('Rebuilding trajectory')
        lattice = cabs.CabsLattice()
        trajectory.coordinates = lattice.rebuild_trafile(trajectory)
        trajectory.template = lattice.rebuild_template(trajectory_template)
        trajectory.LATTICE_UNIT = 1.0

        # align trajectory replicas with the receptor
        logging.info('Aligning trajectory')
        receptor_chains = '+'.join(receptor.chains.keys())
        for i, replica in enumerate(trajectory.trajectories):
            replica.fit_to(receptor.ca, f'name CA and chain {receptor_chains}')
            logging.debug(f'Aligning replica {i + 1}/{len(trajectory.trajectories)}')
            trajectory.coordinates[i] = replica.coordinates

        # calculate contact maps
        logging.info('Calculating contact maps')
        logging.debug(f'Contact cutoff = {self.contact_cutoff}Å')

        receptor_trajectory = trajectory.select(f'chain {receptor_chains} and name SG')
        ligand_trajectory = trajectory.select(f'not chain {receptor_chains} and name SG')

        cmaps = []
        for i in range(trajectory.replicas):
            logging.debug(f'Calculating contact maps for replica {i + 1}')
            for j in range(trajectory.frames):
                cmaps.append(
                    utils.ContactMap(
                        cmap=utils.DistanceMatrix(
                            receptor_trajectory.coordinates[i][j],
                            ligand_trajectory.coordinates[i][j],
                        ).contact_map(self.contact_cutoff),
                        replica=i,
                        frame=j
                    )
                )
        max_contacts = np.max(np.array([cmap.contacts for cmap in cmaps]))
        logging.debug(f'Max number of contacts: {max_contacts}')

        # filter cmaps -> fmaps
        logging.info('Filtering contact maps')
        logging.debug(f'Contact threshold = {self.min_contacts} contacts')
        fmaps = [m for m in cmaps if m.contacts >= self.min_contacts]
        logging.info(f'{len(fmaps)} contact maps were kept')

        # calculate contact matrix
        logging.info('Calculating contact matrix')
        num = len(fmaps)
        cmatrix = np.zeros(shape=(num, num), dtype=np.int8)
        for i in range(num):
            for j in range(i + 1, num):
                cmatrix[i][j] = cmatrix[j][i] = (fmaps[i] & fmaps[j]).count(1)

        # normalize cmatrix diagonal
        max_contacts = cmatrix.max()
        logging.debug(f'Max number of shared contacts: {max_contacts}')
        for i in range(num):
            cmatrix[i][i] = max_contacts

        # calculate distance matrix
        logging.debug('Calculating distance matrix')

        def calc_distance(x):
            return (max_contacts - x) / (max_contacts + x)

        dmatrix = np.vectorize(calc_distance, otypes=[np.half])(cmatrix)

        # clustering
        logging.info(f'Clustering {num} structures')
        medoids, clusters = utils.kmedoids(dmatrix, self.clusters)

        # save rebuilt trajectory
        if self.save_trajectory:
            filename = os.path.join(self.workdir, 'rebuilt.cbs')
            logging.info(f'Saving rebuilt trajectory to "{filename}"')
            trajectory.save(filename)

        # save replicas
        if self.save_replicas:
            logging.info('Saving replicas')
            for index, replica in enumerate(trajectory.trajectories, 1):
                filename = os.path.join(self.workdir, f'replica_{index}.pdb')
                logging.debug(f'Saving "{filename}"')
                with open(filename, 'w') as f:
                    f.write(replica.pdb)

        # save clusters
        if self.save_clusters:
            logging.info('Saving clusters')
            for index in clusters:
                filename = os.path.join(self.workdir, f'cluster_{index + 1}.pdb')
                logging.debug(f'Saving "{filename}"')
                with open(filename, 'w') as f:
                    f.write(
                        atom.Trajectory(
                            template=trajectory.template,
                            coordinates=np.stack([
                                trajectory.coordinates[fmaps[model].replica, fmaps[model].frame, :, :]
                                for model in clusters[index]
                            ])
                        ).pdb
                    )

        # save medoids
        if self.save_medoids:
            logging.info('Saving medoids')
            for index, model in enumerate(medoids, 1):
                filename = os.path.join(self.workdir, f'medoid_{index}.pdb')
                logging.debug(f'Saving "{filename}"')
                with open(filename, 'w') as f:
                    f.write(trajectory[fmaps[model].replica][fmaps[model].frame].pdb)

        if self.reference:
            logging.info("Comparing models with the reference structure")
            filename, r_chains, l_chains = self.reference.split(':')
            logging.debug(f'Loading reference structure from "{filename}"')
            logging.debug(f'Receptor chain(s): "{r_chains}", ligand chain(s): "{l_chains}"')
            chains = '+'.join(r_chains + l_chains)
            reference = atom.Atoms.from_file(filename).select(f'name CA and chain {chains}')

            # reference_tra = reference structure stripped to CA's and cast on the lattice
            reference_tra = trafile.Trafile(
                template=reference,
                coordinates=np.concatenate([
                    lattice.cast(chain.numpy) for chain in reference.chains_list
                ]).reshape(1, 1, -1, 3)
            )

            reference_tra.coordinates = lattice.rebuild_trafile(reference_tra)
            reference_tra.template = lattice.rebuild_template(reference_tra.template)
            reference_tra.LATTICE_UNIT = 1.0

            native_cmap = utils.DistanceMatrix(
                coord1=reference_tra[0].atoms.select(f'chain {r_chains} and name SG').numpy,
                coord2=reference_tra[0].atoms.select(f'chain {l_chains} and name SG').numpy
            ).contact_map(self.contact_cutoff)
            native_contacts = native_cmap.count(1)
            logging.debug(f'Number of native contacts: {native_contacts}')

            nats = np.zeros(shape=(trajectory.replicas, trajectory.frames), dtype=np.int8)
            for cmap in cmaps:
                nats[cmap.replica][cmap.frame] = (cmap.cmap & native_cmap).count(1)

            nat_max = np.max(nats)
            logging.debug(f'Max native contacts = {nat_max}')
            best_inds = list(zip(*np.where(nats == nat_max)))
            best_inds_str = ', '.join(f'{frame + 1}/{replica + 1}' for replica, frame in best_inds)
            logging.debug(f'Best models: {best_inds_str} [frame/replica]')
            with open(os.path.join(self.workdir, 'best.pdb'), 'w') as f:
                f.write(
                    atom.Trajectory(
                        template=trajectory.template,
                        coordinates=np.stack([
                            trajectory.coordinates[replica, frame, :, :] for replica, frame in best_inds
                        ])
                    ).pdb
                )

            np.save(os.path.join(self.workdir, 'cmatrix.npy'), cmatrix)
            np.save(os.path.join(self.workdir, 'cmaps.npy'), fmaps)
            np.save(os.path.join(self.workdir, 'nats.npy'), nats)

    @staticmethod
    def get_atoms(inp):
        try:
            atoms = atom.Atoms.from_file(inp)
        except FileNotFoundError as e:
            try:
                code, chains = inp.split(':')
            except ValueError:
                code = inp
                chains = ''
            try:
                atoms = atom.Atoms.from_code(code)
                if chains:
                    atoms = atoms.select(f'chain {chains.upper()}')
            except atom.InvalidPdbCode:
                raise e
        return atoms

    @staticmethod
    def save_restraints(atoms, filename, weight):

        restraints = atoms.generate_ca_restraints('all', 5, 5.0, 15.0)
        chain_ids = ''.join(atoms.chains.keys())
        res_ids = {chid: list(atoms.chains[chid].residues.keys()) for chid in chain_ids}

        def fix_id(atom_id):
            ch, num = atom_id.split(':')
            return f'{res_ids[ch].index(num) + 1}:PEP{chain_ids.index(ch) + 1}'

        os.makedirs(os.path.dirname(filename), exist_ok=True)
        with open(filename, 'w') as f:
            for restr in restraints:
                id1, id2, dist = restr
                f.write(f'{fix_id(id1)} {fix_id(id2)} {dist:.3f} {weight}\n')

    def run_cabsdock(self):
        cmd = ['CABSdock']
        for arg in self.cabsdock_args:
            cmd.append(arg)
            val = self.cabsdock_args.get(arg)
            if val:
                try:
                    cmd.extend(val.split())
                except AttributeError:
                    cmd.append(str(val))
        return subprocess.run(cmd)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-r', '--receptor',
        required=True,
        help='receptor input'
    )
    parser.add_argument(
        '-l', '--ligand',
        required=True,
        help='ligand input'
    )
    parser.add_argument(
        '-w', '--workdir',
        required=True,
        help='set workdir'
    )

    parser.add_argument(
        '-f', '--ligand-restraints-weight',
        default=0.1,
        metavar='WEIGHT',
        type=float,
        help='set weight of the ligand restraints to %(metavar)s. Default: %(default)s'
    )

    group = parser.add_mutually_exclusive_group()

    group.add_argument(
        '-cd', '--cabsdock',
        action='append',
        metavar='ARG=VAL',
        help='pass argument to CABSdock, can be used multiple times'
    )

    group.add_argument(
        '-L', '--load-cbs',
        metavar='FILE.CBS',
        help='don\'t run CABS simulation, load previously saved trajectory instead'
    )

    parser.add_argument(
        '-c', '--contact-cutoff',
        metavar='VALUE',
        type=float,
        default=4.5,
        help='set contact cutoff in Å. Default: %(default)sÅ'
    )

    parser.add_argument(
        # add percents
        '-m', '--min-contacts',
        metavar='NUMBER',
        type=int,
        default=10,
        help='set minimal number of contacts for structure to be kept. Default: %(default)s'
    )

    parser.add_argument(
        '-k', '--clusters',
        metavar='NUMBER',
        type=int,
        default=10,
        help='set number of clusters. Default: %(default)s'
    )

    parser.add_argument(
        '-o', '--output-pdb',
        metavar='OPTIONS',
        default='A',
        help='choose which pdb files will be saved: (T)rajectory (R)eplicas, (C)lusters, (M)edoids, (A)ll, (N)one. '
             'Default is %(default)s. Example: -o RC will save replicas and clusters.'
    )

    parser.add_argument(
        '-R', '--reference',
        metavar='[filename]:[receptor chain(s)]:[ligand chain(s)]',
        help='compare models to the reference structure, this will save the fnats.npy file in the working directory'
    )

    args = parser.parse_args()
    logging.basicConfig(
        format='%(levelname)s:%(message)s',
        level='DEBUG',
    )
    app = App(**vars(args))
    app.run()
