#! /bin/bash

src=${BASH_SOURCE[0]}

if [[ -L ${src} ]]; then
    src=$(file ${src} | awk '{print $NF}')
fi

project_dir=$(cd `dirname ${src}`/.. && pwd)

source $project_dir/venv/bin/activate
export PYTHONPATH=$project_dir

id=$1
workdir=$project_dir/playground/$id
python $project_dir/scripts/dock.py -r 1a2p:A -l 1a19:A -w $workdir -L $workdir/trajectory.cbs -R $project_dir/test/1brs.pdb:C:F -m 1 -c 4.5 -oN >$workdir/out.txt 2>$workdir/log.txt
