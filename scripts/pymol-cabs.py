from pymol import cmd


def as_cabs(mol):

    # remove all bonds
    cmd.unbond(mol, '*')

    # initialize storage dict
    storage = {}

    # save residue indices in storage
    for chain_id in cmd.get_chains(mol):
        storage[chain_id] = []
        cmd.iterate(
            '(%s and chain %s and name ca)' % (mol, chain_id),
            '%s.append(resi)' % chain_id,
            space=storage
        )

    # iterate over all chains
    for chain_id in storage:
        for i in range(1, len(storage[chain_id])):
            cmd.select('prev', '%s and chain %s and resi %s and name ca' % (
                mol, chain_id, storage[chain_id][i - 1]
            ))
            cmd.select('curr', '%s and chain %s and resi %s and name ca' % (
                mol, chain_id, storage[chain_id][i]
            ))
            cmd.bond('prev', 'curr')

        for resnum in storage[chain_id]:
            cmd.select('ca', '%s and chain %s and resi %s and name ca' % (
                mol, chain_id, resnum))
            cmd.select('cb', '%s and chain %s and resi %s and name cb' % (
                mol, chain_id, resnum))
            cmd.select('sg', '%s and chain %s and resi %s and name sg' % (
                mol, chain_id, resnum))
            cmd.bond('ca', 'cb')
            cmd.bond('cb', 'sg')

    for s in ['prev', 'curr', 'ca', 'cb', 'sg']:
        cmd.delete(s)

    cmd.show_as('sticks', mol)
    cmd.color('green', '%s and name ca' % mol)
    cmd.color('blue', '%s and name cb' % mol)
    cmd.color('red', '%s and name sg' % mol)
    # cmd.show('spheres', mol)
    # cmd.set('sphere_scale', 0.2, mol)
    # cmd.set('sphere_scale', 0.6, '%s and name sg' % mol)
    # cmd.set('sphere_transparency', 0.5, '%s and name sg' % mol)


def all_cabs():
    for mol in cmd.get_object_list():
        as_cabs(mol)
        print('%s done' % mol)


def get_contacts(mol, distance=4.5):

    storage = {'sgs': []}
    cmd.iterate(
        selection='name SG',
        expression='sgs.append((chain, resi))',
        space=storage
    )

    for i in range(len(storage['sgs'])):
        ch1, res1 = storage['sgs'][i]
        cmd.select('s1', 'chain %s and resi %s and name SG' % (ch1, res1))
        for j in range(i + 1, len(storage['sgs'])):
            ch2, res2 = storage['sgs'][j]
            if ch2 != ch1:
                cmd.select('s2', 'chain %s and resi %s and name SG' % (ch2, res2))
                d = cmd.get_distance('s1', 's2')
                if d < distance:
                    cmd.show('spheres', 's1')
                    cmd.show('spheres', 's2')
                    cmd.set('sphere_scale', 0.3, 's1 or s2')
                    cmd.color('yellow', 's1 or s2')
                    cmd.distance('%s%s_%s%s' % (ch1, res1, ch2, res2), 's1', 's2')

    cmd.delete('s1')
    cmd.delete('s2')
