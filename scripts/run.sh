#!/bin/bash

#PBS -l walltime=20:00:00
#PBS -o /STORAGE/DATA/mkurc/logs/stdout_${PBS_JOBID}
#PBS -e /STORAGE/DATA/mkurc/logs/stderr_${PBS_JOBID}

HOME_DIR=${HOME}/CABSprot
source ${HOME_DIR}/venv/bin/activate
export PYTHONPATH=${HOME_DIR}

receptor=${HOME_DIR}/data/receptor.pdb
ligand=${HOME_DIR}/data/ligand.pdb

frest=$1
scale=$2
workdir=/STORAGE/DATA/mkurc/runs/$1"_"$2

python ${HOME_DIR}/scripts/dock.py -r ${receptor} -l ${ligand} -w ${workdir} -f $1 -cd b=$2 -cd a=5 -cd y=5 -cd s=5 -cd r=5
