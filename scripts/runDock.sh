#! /bin/bash

src=${BASH_SOURCE[0]}

if [[ -L ${src} ]]; then
    src=$(file ${src} | awk '{print $NF}')
fi

project_dir=$(cd `dirname ${src}`/.. && pwd)

source $project_dir/venv/bin/activate
export PYTHONPATH=$project_dir

receptor=$1
ligand=$2
workdir=$project_dir/benchmark/$3
log=$project_dir/benchmark/$3".log"

python $project_dir/scripts/dock.py -r $receptor -l $ligand -w $workdir -cd r=5 -cd a=5 -cd y=5 -cd s=5 2>$log
