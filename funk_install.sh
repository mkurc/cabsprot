#! /bin/bash

virtualenv -p python2.7 cabs-env
source cabs-env/bin/activate
pip install /home/users/mkurc/CABSdock
deactivate
virtualenv -p python3.7 venv
cp /home/users/mkurc/CABSprot/venv/bin/CABSdock venv/bin/CABSdock
source venv/bin/activate
pip install -r requirements.txt
export PYTHONPATH='.'
python scripts/dock.py
CABSdock --ver